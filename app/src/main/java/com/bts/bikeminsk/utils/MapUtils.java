package com.bts.bikeminsk.utils;

import android.content.Context;
import android.preference.PreferenceManager;

import com.bts.bikeminsk.R;

public class MapUtils {

    public static String getMapUrl(int mapIndex) {
        switch (mapIndex) {
            case 0:
                return "mapbox://styles/alexmo/cjvzlhq921dwt1cmyvasc6oca";
            case 1:
                return "mapbox://styles/alexmo/cjwaidibq02au1ctfqiya7oco";
            default:
                return "mapbox://styles/alexmo/cjvzlhq921dwt1cmyvasc6oca";
        }
    }

    public static int getMapTypeIndex(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(context.getString(R.string.pr_map_type_index), 0);
    }

    public static void setMapTypeIndex(Context context, int mapTypeIndex) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putInt(context.getString(R.string.pr_map_type_index), mapTypeIndex).apply();
    }

}
