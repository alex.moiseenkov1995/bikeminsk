package com.bts.bikeminsk.mapviewmanager;

public enum TypePOI {
    Sight,
    BikeRent,
    BikeService,
    Food
}
