package com.bts.bikeminsk.mapviewmanager;

public interface MapEventsListener {

    void onMapStyleChanged();

    void onMapTouch();

    void onPOIClick(POI poi);

}