package com.bts.bikeminsk.mapviewmanager;

import android.os.Parcel;
import android.os.Parcelable;

import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;

public class POI implements Parcelable {
    public int id;
    public double latitude;
    public double longitude;
    public TypePOI typePOI;
    public String name;
    public String description;
    public String phone;
    public String url;
    public String photo;
    public String worktime;

    public POI(Feature feature, TypePOI typePOI) {

        this.typePOI = typePOI;

        if (feature.geometry() instanceof Point) {
            this.latitude = ((Point) feature.geometry()).latitude();
            this.longitude = ((Point) feature.geometry()).longitude();
        }

        if (feature.hasProperty("Id")) {
            this.id = feature.getNumberProperty("Id").intValue();
        }

        if (feature.hasProperty("name")) {
            this.name = feature.getStringProperty("name");
        }
        if (feature.hasProperty("descriptio")) {
            this.description = feature.getStringProperty("descriptio");
        }
        if (feature.hasProperty("phone")) {
            this.phone = feature.getStringProperty("phone");
        }
        if (feature.hasProperty("url")) {
            this.url = feature.getStringProperty("url");
        }
        if (feature.hasProperty("photo")) {
            this.photo = feature.getStringProperty("photo");
        }
        if (feature.hasProperty("work")) {
            this.worktime = feature.getStringProperty("work");
        }
    }

    protected POI(Parcel in) {
        id = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        name = in.readString();
        description = in.readString();
        phone = in.readString();
        url = in.readString();
        photo = in.readString();
        worktime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(phone);
        dest.writeString(url);
        dest.writeString(photo);
        dest.writeString(worktime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<POI> CREATOR = new Creator<POI>() {
        @Override
        public POI createFromParcel(Parcel in) {
            return new POI(in);
        }

        @Override
        public POI[] newArray(int size) {
            return new POI[size];
        }
    };

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("POI{");
        sb.append("id=").append(id);
        sb.append(", latitude=").append(latitude);
        sb.append(", longitude=").append(longitude);
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", photo='").append(photo).append('\'');
        sb.append(", worktime='").append(worktime).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
