package com.bts.bikeminsk.mapviewmanager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.RectF;
import android.view.MotionEvent;

import com.bts.bikeminsk.views.CustomMapBoxView;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.localization.LocalizationPlugin;
import com.mapbox.mapboxsdk.plugins.localization.MapLocale;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;


public class MapManager implements
        MapboxMap.OnMapClickListener,
        CustomMapBoxView.OnMapTouchListener {

    private static final int ANIMATION_LENGTH = 800;
    public static final float COMFORT_ZOOM_LEVEL = 11;

    private static final String SIGHTS_LAYER_ID = "sights";
    private static final String BIKE_RENT_LAYER_ID = "velobikes";
    private static final String BIKE_SERVICE_LAYER_ID = "veloservice";
    private static final String FOOD_LAYER_ID = "food";

    private Context context;
    private MapboxMap mapboxMap;
    private Style mapboxStyle;
    private LocationComponent locationComponent;
    private MapEventsListener mapEventsListener;


    public MapManager(final Context context, final MapboxMap map, Style mapStyle, CustomMapBoxView mapView, MapEventsListener mapEventsListener) {
        this.context = context;
        this.mapEventsListener = mapEventsListener;
        mapboxMap = map;
        mapboxStyle = mapStyle;
        mapboxMap.setMaxZoomPreference(20);
        mapboxMap.addOnMapClickListener(this);
        mapboxMap.getUiSettings().setLogoEnabled(false);
        mapboxMap.getUiSettings().setAttributionEnabled(false);
        mapView.setOnMapTouchListener(this);

        setMapLocale(mapboxMap, mapView);
        initUserLocation();
    }


    @Override
    public boolean onMapClick(@NonNull LatLng point) {

        final PointF pixel = mapboxMap.getProjection().toScreenLocation(point);
        RectF rectF = new RectF(pixel.x - 40, pixel.y - 40, pixel.x + 40, pixel.y + 40);

        List<Feature> sightsFeatures = mapboxMap.queryRenderedFeatures(rectF, SIGHTS_LAYER_ID);
        List<Feature> bikeRentFeatures = mapboxMap.queryRenderedFeatures(rectF, BIKE_RENT_LAYER_ID);
        List<Feature> bikeServiceFeatures = mapboxMap.queryRenderedFeatures(rectF, BIKE_SERVICE_LAYER_ID);
        List<Feature> foodFeatures = mapboxMap.queryRenderedFeatures(rectF, FOOD_LAYER_ID);

        if (sightsFeatures.size() > 0) {
            onPOIClick(new POI(sightsFeatures.get(0), TypePOI.Sight));
            return true;
        }
        if (bikeRentFeatures.size() > 0) {
            onPOIClick(new POI(bikeRentFeatures.get(0), TypePOI.BikeRent));
            return true;
        }
        if (bikeServiceFeatures.size() > 0) {
            onPOIClick(new POI(bikeServiceFeatures.get(0), TypePOI.BikeService));
            return true;
        }
        if (foodFeatures.size() > 0) {
            onPOIClick(new POI(foodFeatures.get(0), TypePOI.Food));
            return true;
        }
        return false;
    }

    public ArrayList<POI> getSights(@NonNull LatLng point) {

        final PointF pixel = mapboxMap.getProjection().toScreenLocation(point);
        RectF rectF = new RectF(pixel.x - 40, pixel.y - 40, pixel.x + 40, pixel.y + 40);

        List<Feature> sightsFeatures = mapboxMap.queryRenderedFeatures(rectF, SIGHTS_LAYER_ID);
        List<Feature> bikeRentFeatures = mapboxMap.queryRenderedFeatures(rectF, BIKE_RENT_LAYER_ID);
        List<Feature> bikeServiceFeatures = mapboxMap.( BIKE_SERVICE_LAYER_ID);
        List<Feature> foodFeatures = mapboxMap.queryRenderedFeatures(rectF, FOOD_LAYER_ID);

        if (sightsFeatures.size() > 0) {
            onPOIClick(new POI(sightsFeatures.get(0), TypePOI.Sight));
            return true;
        }
        if (bikeRentFeatures.size() > 0) {
            onPOIClick(new POI(bikeRentFeatures.get(0), TypePOI.BikeRent));
            return true;
        }
        if (bikeServiceFeatures.size() > 0) {
            onPOIClick(new POI(bikeServiceFeatures.get(0), TypePOI.BikeService));
            return true;
        }
        if (foodFeatures.size() > 0) {
            onPOIClick(new POI(foodFeatures.get(0), TypePOI.Food));
            return true;
        }
        return false;
    }

    @Override
    public void onMapViewTouch(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (mapEventsListener != null) {
                mapEventsListener.onMapTouch();
            }
        }
    }

    private void onPOIClick(POI poi) {
        if (mapEventsListener != null) {
            mapEventsListener.onPOIClick(poi);
        }
    }


    public void setMapStyle(String styleUrl) {
        mapboxMap.setStyle(styleUrl, style -> {
            mapboxStyle = style;
            mapEventsListener.onMapStyleChanged();
        });
    }

    public void zoomIn() {
        CameraPosition.Builder builder = new CameraPosition.Builder();
        builder.zoom(mapboxMap.getCameraPosition().zoom + 1);
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
    }

    public void zoomOut() {
        CameraPosition.Builder builder = new CameraPosition.Builder();
        builder.zoom(mapboxMap.getCameraPosition().zoom - 1);
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
    }

    public float getZoom() {
        return (float) mapboxMap.getCameraPosition().zoom;
    }

    public void animateTo(POI poi, float zoomLevel, int bottomPadding) {
        //set padding
        mapboxMap.setPadding(0, 0, 0, bottomPadding);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(poi.latitude, poi.longitude))
                .zoom(zoomLevel)
                .build();
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), ANIMATION_LENGTH);
    }

    private Context getContext() {
        return context;
    }


    @SuppressLint("MissingPermission")
    private void initUserLocation() {
        if (!PermissionsManager.areLocationPermissionsGranted(getContext())) {
            return;
        }

        locationComponent = mapboxMap.getLocationComponent();

        LocationComponentActivationOptions locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(getContext(), mapboxStyle)
                        .build();

        locationComponent.activateLocationComponent(locationComponentActivationOptions);
        locationComponent.setLocationComponentEnabled(true);
        locationComponent.setCameraMode(CameraMode.NONE);
        locationComponent.setRenderMode(RenderMode.COMPASS);
    }

    @SuppressWarnings({"MissingPermission"})
    public void setTrackCurrentLocationMode() {
        if (locationComponent == null) {
            initUserLocation();
        }
        if (locationComponent != null) {
            locationComponent.setLocationComponentEnabled(true);
            locationComponent.setCameraMode(CameraMode.TRACKING);
        }
    }


    private void setMapLocale(MapboxMap mapboxMap, MapView mapView) {
        LocalizationPlugin localizationPlugin = new LocalizationPlugin(mapView, mapboxMap, mapboxStyle);
        switch (Locale.getDefault().getLanguage()) {
            case "pl":
            case "en":
                localizationPlugin.setMapLanguage(MapLocale.ENGLISH);
                break;
            case "es":
                localizationPlugin.setMapLanguage(MapLocale.SPANISH);
                break;
            case "fr":
                localizationPlugin.setMapLanguage(MapLocale.FRENCH);
                break;
            case "de":
                localizationPlugin.setMapLanguage(MapLocale.GERMAN);
                break;
            case "ru":
                localizationPlugin.setMapLanguage(MapLocale.RUSSIAN);
                break;
            case "zh":
                localizationPlugin.setMapLanguage(MapLocale.CHINESE);
                break;
            case "pt":
                localizationPlugin.setMapLanguage(MapLocale.PORTUGUESE);
                break;
            case "ar":
                localizationPlugin.setMapLanguage(MapLocale.ARABIC);
                break;
            case "ja":
                localizationPlugin.setMapLanguage(MapLocale.JAPANESE);
                break;
            case "ko":
                localizationPlugin.setMapLanguage(MapLocale.KOREAN);
                break;
            default:
                localizationPlugin.setMapLanguage(MapLocale.ENGLISH);
                break;
        }
    }
}
