package com.bts.bikeminsk.views.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bts.bikeminsk.R;
import com.bts.bikeminsk.utils.MapUtils;

public class SelectMapTypeAdapter extends ArrayAdapter<String> {

    private String[] mapTypes;
    private int selectedItem;

    public SelectMapTypeAdapter(Context context, String[] mapTypes) {
        super(context, android.R.layout.simple_list_item_1, mapTypes);
        this.mapTypes = mapTypes;
        selectedItem = MapUtils.getMapTypeIndex(context);
    }

    @NonNull
    @Override
    public View getView(int position, View v, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.map_type_item, null);
        TextView tvItem = v.findViewById(R.id.tvItem);
        tvItem.setText(mapTypes[position]);
        if (this.selectedItem == position) {
            tvItem.setTextColor(getContext().getResources().getColor(R.color.colorAccent));
        }
        return v;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    public void setSelectedItem(int position) {
        this.selectedItem = position;
    }

    public int getSelectedItem() {
        return selectedItem;
    }
}