package com.bts.bikeminsk.views;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;

public class CustomMapBoxView extends MapView {
    private OnMapTouchListener touchListener;

    public CustomMapBoxView(@NonNull Context context) {
        super(context);
    }

    public CustomMapBoxView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomMapBoxView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomMapBoxView(@NonNull Context context, @Nullable MapboxMapOptions options) {
        super(context, options);
    }

    public void setOnMapTouchListener(OnMapTouchListener listener) {
        this.touchListener = listener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (touchListener != null) {
            touchListener.onMapViewTouch(event);
        }
        return super.onTouchEvent(event);
    }

    public interface OnMapTouchListener {
        public void onMapViewTouch(MotionEvent event);
    }
}
