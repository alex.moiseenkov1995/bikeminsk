package com.bts.bikeminsk.activities;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bts.bikeminsk.R;
import com.bts.bikeminsk.mapviewmanager.POI;
import com.bumptech.glide.Glide;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static com.google.common.base.Strings.isNullOrEmpty;

public class POIInfoFragment extends Fragment {
    private final static String KEY_POI = "key_poi";

    private POIInfoCallBack poiInfoCallBack;

    private POI poi;

    private TextView tvName;
    private TextView tvType;
    private TextView tvDescription;
    private View containerPhone;
    private TextView tvPhone;
    private View containerUrl;
    private TextView tvUrl;
    private View containerPhoto;
    private ImageView ivPhoto;
    private TextView tvWorktime;


    public static POIInfoFragment newInstance() {
        POIInfoFragment fragment = new POIInfoFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    public static POIInfoFragment newInstance(POI poi) {
        POIInfoFragment fragment = new POIInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_POI, poi);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        poi = getArguments().getParcelable(KEY_POI);
        poiInfoCallBack = ((POIInfoCallBack) requireActivity());
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_poi_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvName = view.findViewById(R.id.tvName);
        tvType = view.findViewById(R.id.tvType);
        tvDescription = view.findViewById(R.id.tvDescription);
        containerPhone = view.findViewById(R.id.containerPhone);
        tvPhone = view.findViewById(R.id.tvPhone);
        containerUrl = view.findViewById(R.id.containerUrl);
        tvUrl = view.findViewById(R.id.tvUrl);
        containerPhoto = view.findViewById(R.id.containerPhoto);
        ivPhoto = view.findViewById(R.id.ivPhoto);
        tvWorktime = view.findViewById(R.id.tvWorktime);

        view.findViewById(R.id.ivClose).setOnClickListener(v -> {
            if (poiInfoCallBack != null) {
                poiInfoCallBack.onCloseClick();
            }
        });

        view.findViewById(R.id.btnBuildRoute).setOnClickListener(v -> {
            if (poiInfoCallBack != null && poi != null) {
                buildRouteInOtherApp(String.valueOf(poi.latitude), String.valueOf(poi.longitude));
//                poiInfoCallBack.onBuildRouteClick(poi);
            }
        });

        containerPhone.setOnClickListener(v -> {
            if (poiInfoCallBack != null && poi != null && !isNullOrEmpty(poi.phone)) {
                makeCall(poi.phone);
            }
        });

        containerUrl.setOnClickListener(v -> {
            if (poiInfoCallBack != null && poi != null && !isNullOrEmpty(poi.url)) {
                openInBrowser(poi.url);
            }
        });

        setPOIInfo();
    }

    public void updatePOI(POI poi) {
        this.poi = poi;
        setPOIInfo();
    }

    private void setPOIInfo() {
        if (poi == null) {
            return;
        }
        if (!isNullOrEmpty(poi.name)) {
            tvName.setVisibility(View.VISIBLE);
            tvName.setText(poi.name);
        } else {
            tvName.setVisibility(View.GONE);
        }

        if (poi.typePOI != null) {
            switch (poi.typePOI) {
                case Sight:
                    tvType.setText("Достопримечательности");
                    break;
                case BikeRent:
                    tvType.setText("Аренда велосипедов");
                    break;
                case BikeService:
                    tvType.setText("Велосервис");
                    break;
                case Food:
                    tvType.setText("Еда,напитки");
                    break;
                default:
                    tvType.setText("");
            }

        } else {
            tvType.setText("");
        }


        if (!isNullOrEmpty(poi.worktime)) {
            tvWorktime.setText(poi.worktime);
        } else {
            tvWorktime.setText("График работы не указан");
        }

        if (!isNullOrEmpty(poi.phone)) {
            containerPhone.setVisibility(View.VISIBLE);
            tvPhone.setText(poi.phone);
        } else {
            containerPhone.setVisibility(View.GONE);
        }

        if (!isNullOrEmpty(poi.url)) {
            containerUrl.setVisibility(View.VISIBLE);
            tvUrl.setText(poi.url);
        } else {
            containerUrl.setVisibility(View.GONE);
        }


        if (!isNullOrEmpty(poi.photo)) {
            containerPhoto.setVisibility(View.VISIBLE);

            Glide.with(requireContext())
                    .load(poi.photo)
                    .centerCrop()
                    .into(ivPhoto);
        } else {
            containerPhoto.setVisibility(View.GONE);
        }

        if (!isNullOrEmpty(poi.description)) {
            tvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(poi.description);
        } else {
            tvDescription.setVisibility(View.GONE);
        }
    }

    private void openInBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    private void makeCall(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }

    private void buildRouteInOtherApp(String lat, String lon) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?" +
                        "daddr=" +
                        lat + "," +
                        lon));
        startActivity(intent);
    }


    public interface POIInfoCallBack {
        void onCloseClick();

        void onBuildRouteClick(POI poi);
    }
}

