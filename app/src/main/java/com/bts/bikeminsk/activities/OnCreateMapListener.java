package com.bts.bikeminsk.activities;


import com.bts.bikeminsk.mapviewmanager.MapManager;

public interface OnCreateMapListener {
    public void onCreateMap(MapManager mapViewManager);
}