package com.bts.bikeminsk.activities;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bts.bikeminsk.R;
import com.bts.bikeminsk.mapviewmanager.MapEventsListener;
import com.bts.bikeminsk.mapviewmanager.MapManager;
import com.bts.bikeminsk.utils.MapUtils;
import com.bts.bikeminsk.views.CustomMapBoxView;
import com.mapbox.mapboxsdk.Mapbox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static com.bts.bikeminsk.utils.MapUtils.getMapUrl;

public class MapBoxFragment extends Fragment {
    private CustomMapBoxView mapView;
    private OnCreateMapListener mapCreateListener;

    public static MapBoxFragment newInstance() {
        return new MapBoxFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof OnCreateMapListener) {
            mapCreateListener = (OnCreateMapListener) getActivity();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mapCreateListener = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(requireContext(), getString(R.string.mapbox_access_token));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mapbox, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = view.findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(mapboxMap ->
                mapboxMap.setStyle(getMapUrl(MapUtils.getMapTypeIndex(requireContext())), style -> {
                    if (mapCreateListener != null) {
                        MapEventsListener mapEventsListener = (MapEventsListener) getActivity();
                        mapCreateListener.onCreateMap(new MapManager(getContext(), mapboxMap, style, mapView, mapEventsListener));
                    }
                }));
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    //Do not override mapbox onSaveInstanceState() because the map does not restore its state as desired
//    @Override
//    public void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
//        mapView.onSaveInstanceState(outState);
//    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}

