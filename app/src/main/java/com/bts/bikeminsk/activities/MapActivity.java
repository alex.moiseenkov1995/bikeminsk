package com.bts.bikeminsk.activities;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.bts.bikeminsk.R;
import com.bts.bikeminsk.geofence.GeofenceBroadcastReceiver;
import com.bts.bikeminsk.mapviewmanager.MapEventsListener;
import com.bts.bikeminsk.mapviewmanager.MapManager;
import com.bts.bikeminsk.mapviewmanager.POI;
import com.bts.bikeminsk.views.adapters.SelectMapTypeAdapter;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import static com.bts.bikeminsk.mapviewmanager.MapManager.COMFORT_ZOOM_LEVEL;
import static com.bts.bikeminsk.utils.MapUtils.getMapUrl;
import static com.bts.bikeminsk.utils.MapUtils.setMapTypeIndex;
import static com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_HIDDEN;

public class MapActivity extends AppCompatActivity
        implements
        OnCreateMapListener,
//        RouteInfoFragment.RouteInfoCallBack,
        View.OnClickListener,
        MapEventsListener,
        PermissionsListener,
        POIInfoFragment.POIInfoCallBack,
        OnCompleteListener<Void> {

    private static final int REQUEST_PERMISSIONS_ACCESS_FINE_LOCATION = 101;
    private static final int REQUEST_SETTING_GPS = 333;
    private static final int REQUEST_PLAY_SERVICES_RESOLUTION = 222;
    public static final String POI_INFO_FRAGMENT_TAG = "poi_info_fragment_tag";

    public static String KEY_DEVICE = "key_device";
    private static String ORIENTATION_CHANGE = "orientation_change";
    private static String KEY_NAVIGATION_MARKER = "key_navigation_marker";
    private static String LOCATION_FOLLOW = "location_follow";
    private static String FIRST_OPEN = "first_open";
    private static String LOCATION_ENABLED = "location_enabled";
    private static String DEVICE_ID = "device_id";
    public static String KEY_LAST_ANCHOR_STATE = "key_last_anchor_state";
    public static String KEY_LAST_CAMERA_POSITION = "key_last_camera_position";

    private ImageButton btnLocation;
    private View ibChangeMap;
    private View chooseMap;

    private PermissionsManager permissionsManager;
    private MapManager mapManager;
    private SelectMapTypeAdapter selectMapTypeAdapter;
    private BottomSheetBehavior sheetBehavior;


    private GeofencingClient mGeofencingClient;
    private PendingIntent mGeofencePendingIntent;
    private ArrayList<Geofence> mGeofenceList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
//        if (savedInstanceState == null) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mapFragment, MapBoxFragment.newInstance())
                .replace(R.id.poiInfoFragment, POIInfoFragment.newInstance(), POI_INFO_FRAGMENT_TAG)
                .commit();
//        }


        //init BottomSheet
        View layoutBottomSheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        sheetBehavior.setState(STATE_HIDDEN);


        findViewById(R.id.btnZoomIn).setOnClickListener(this);
        findViewById(R.id.btnZoomOut).setOnClickListener(this);
        chooseMap = findViewById(R.id.chooseMap);
        ibChangeMap = findViewById(R.id.ibChangeMap);
        ibChangeMap.setOnClickListener(this);
        btnLocation = findViewById(R.id.btnLocation);
        btnLocation.setOnClickListener(this);


        //init views for manipulating map style
        ListView mapItemsView = findViewById(R.id.map_items);
        selectMapTypeAdapter = new SelectMapTypeAdapter(this, getResources().getStringArray(R.array.map_type));
        mapItemsView.setAdapter(selectMapTypeAdapter);
        mapItemsView.setDivider(null);
        mapItemsView.setOnItemClickListener((parent, view1, position, id) -> {
            if (selectMapTypeAdapter.getSelectedItem() == position) {
                return;
            }
            if (mapManager == null) {
                return;
            }
            selectMapTypeAdapter.setSelectedItem(position);
            selectMapTypeAdapter.notifyDataSetChanged();
            setMapTypeIndex(this, position);
            mapManager.setMapStyle(getMapUrl(position));
        });
    }


    /**
     * Adds geofences, which sets alerts to be notified when the device enters or exits one of the
     * specified geofences. Handles the success or failure results returned by addGeofences().
     */
    public void addGeofencesButtonHandler(View view) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            addGeofences();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    /**
     * Adds geofences. This method should be called after the user has granted the location
     * permission.
     */
    @SuppressWarnings("MissingPermission")
    private void addGeofences() {
        if (mGeofencingClient == null) {
            mGeofencingClient = LocationServices.getGeofencingClient(this);
        }
        populateGeofenceList();
        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnCompleteListener(this);
    }

    private void populateGeofenceList() {

        if (mGeofenceList == null || mGeofenceList.size() == 0){
            mGeofenceList = new ArrayList<>();
        }

        mGeofenceList.clear();

        for (Map.Entry<String, LatLng> entry : Constants.BAY_AREA_LANDMARKS.entrySet()) {

            mGeofenceList.add(new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this
                    // geofence.
                    .setRequestId(entry.getKey())

                    // Set the circular region of this geofence.
                    .setCircularRegion(
                            entry.getValue().latitude,
                            entry.getValue().longitude,
                            Constants.GEOFENCE_RADIUS_IN_METERS
                    )

                    // Set the expiration duration of the geofence. This geofence gets automatically
                    // removed after this period of time.
                    .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)

                    // Set the transition types of interest. Alerts are only generated for these
                    // transition. We track entry and exit transitions in this sample.
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)

                    // Create the geofence.
                    .build());
        }
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceBroadcastReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }


    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {

        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    @Override
    public void onComplete(@NonNull Task<Void> task) {
        if (task.isSuccessful()) {
            Toast.makeText(this, "Успешно", Toast.LENGTH_SHORT).show();
        } else {

        }
    }



    @Override
    public void onCreateMap(MapManager manager) {
        mapManager = manager;
    }


    @Override
    public void onMapStyleChanged() {

    }

    @Override
    public void onMapTouch() {
        setChooseMapVisibility(false);
    }

    @Override
    public void onPOIClick(POI poi) {
        POIInfoFragment poiInfoFragment = (POIInfoFragment) getSupportFragmentManager().findFragmentByTag(POI_INFO_FRAGMENT_TAG);
        if (poiInfoFragment != null) {
            poiInfoFragment.updatePOI(poi);
        }
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        mapManager.animateTo(poi, Math.max(mapManager.getZoom(), COMFORT_ZOOM_LEVEL), (int) (getResources().getDisplayMetrics().density * 130));
    }


    private void setChooseMapVisibility(boolean visible) {
        chooseMap.setVisibility(visible ? View.VISIBLE : View.GONE);
        ibChangeMap.setVisibility(visible ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnZoomIn:
                mapManager.zoomIn();
                setChooseMapVisibility(false);
                break;
            case R.id.btnZoomOut:
                mapManager.zoomOut();
                setChooseMapVisibility(false);
                break;
            case R.id.btnLocation:
                setTrackCurrentLocationMode();
                setChooseMapVisibility(false);
                break;
            case R.id.ibChangeMap:
                setChooseMapVisibility(true);
                break;
        }
    }

    private void setTrackCurrentLocationMode() {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            if (mapManager != null) {
                mapManager.setTrackCurrentLocationMode();
            }
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            setTrackCurrentLocationMode();
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onCloseClick() {
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    @Override
    public void onBuildRouteClick(POI poi) {

    }


//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        Bundle bundle = new Bundle();
//
//        PreferenceUtils.setTrackResponse(getContext(), trackResponse);
//        bundle.putParcelable(KEY_NAVIGATION_MARKER, navigationMarker);
//        bundle.putParcelable(DEVICE_ID, mSelectDevice);
//        bundle.putBoolean(LOCATION_ENABLED, isLocationEnabled);
//        bundle.putBoolean(LOCATION_FOLLOW, isFollowLocation);
//        bundle.putBoolean(FIRST_OPEN, isFirstOpen);
//
//        //don'' save dragging and settling states
//        int anchorState = sheetBehavior.getState();
//        bundle.putInt(KEY_LAST_ANCHOR_STATE, (anchorState != STATE_DRAGGING && anchorState != STATE_SETTLING) ? anchorState : STATE_COLLAPSED);
//        if (mapManager != null) {
//            bundle.putParcelable(KEY_LAST_CAMERA_POSITION, mapManager.getCameraPosition());
//        }
//        outState.putBundle(ORIENTATION_CHANGE, bundle);
//        super.onSaveInstanceState(outState);
//    }

//
//    private void checkImageLocationButton() {
////        if (isFollowLocation & isLocationEnabled) {
////            btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_on_center));
////        } else if (!isFollowLocation & !isLocationEnabled) {
////            btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_off));
////        } else {
////            btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_on));
////        }
//    }

}

